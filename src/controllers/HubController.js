const HubLink = require('../models/Hub/Link');
const Response = require('../utils/response');

/**
 * Get hub links list
 * @param req
 * @param res
 */
module.exports.getAll = async (req, res) => {
  try {
    const hubLinks = await HubLink.find()
      .select('group url description _id')
      .sort({ _id: -1 })
      .exec();

    return Response.success(res, { hubLinks });
  } catch (err) {
    return Response.errors.internalError(res, err)
  }
};

/**
 * Find and return hub link by id
 * @param req
 * @param res
 */
module.exports.getLinkById = async (req, res) => {
  try {
    const hubLink = await HubLink.findById(req.params.id, 'group url description _id');

    return Response.success(res, { hubLink });
  } catch (err) {
    return Response.errors.internalError(res, err);
  }
};

/**
 * Create hub link
 * @param req
 * @param res
 */
module.exports.createLink = async (req, res) => {
  const newHubLink = new HubLink({
    group: req.body.group,
    url: req.body.url,
    description: req.body.description,
  });

  try{
    const createdHubLink = await newHubLink.save();

    return Response.success(res, {
      message: `Link with ID ${createdHubLink._id} successfully saved`,
      createdHubLink,
    });
  } catch (err) {
    return Response.errors.internalError(res, err);
  }
};

/**
 * Update exists link
 * @param req
 * @param res
 */
module.exports.updateLink = async (req, res) => {
  try {
    const link = await HubLink.findById(req.body.id);

    // Update fields
    if (req.body.group) {
      link.group = req.body.group;
    }
    if (req.body.url) {
      link.url = req.body.url;
    }
    if (req.body.description) {
      link.description = req.body.description;
    }
    // Update post
    try{
      const updatedLink = await link.save();

      return Response.success(res, {
        message: `Link with ID ${updatedLink._id} successfully updated`,
        updatedLink,
      });
    } catch (err) {
      return Response.errors.internalError(res, err);
    }
  } catch (err) {
    return Response.errors.internalError(res, err);
  }
};

/**
 * Delete link by id
 * @param req
 * @param res
 */
module.exports.deleteLink = async (req, res) => {
  try {
    const deletedLink = await HubLink.findOneAndRemove({ _id: req.body.id });
    return Response.success(res, {
      message: `Link with ID ${deletedLink._id} successfully deleted`,
      deletedLink
    });
  } catch (err) {
    return Response.errors.internalError(res, err);
  }
};
